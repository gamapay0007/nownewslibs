package com.nownews.news2.architecture.core.ui.extensions

import android.widget.LinearLayout
import com.google.android.material.tabs.TabLayout

/**
 * Created by AlexYang on 2021/10/2.
 *
 */


/**
 * 隱藏TabLayout第一個TabItem
 */
val TabLayout.hideFirstTab: Unit?
    get() = (getTabAt(0)?.view as? LinearLayout)?.gone
