package com.nownews.news2.architecture.core.ui.extensions

import android.text.TextUtils
import android.widget.TextView
import androidx.core.text.HtmlCompat

/**
 * Created by AlexYang on 2021/8/2.
 *
 */


/**
 * 顯示AppVersionName
 */
fun TextView.appVersionName(versionName: String) {
    text = "v%s".format(versionName)
}

/**
 * 設定HTML格式的文字
 */
fun TextView.htmlText(value: String?) {
    text = if (!TextUtils.isEmpty(value)) HtmlCompat.fromHtml(value!!, HtmlCompat.FROM_HTML_MODE_LEGACY) else ""
}
