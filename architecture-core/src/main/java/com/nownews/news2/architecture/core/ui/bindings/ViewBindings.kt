package com.nownews.news2.architecture.core.ui.bindings

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.transition.Fade
import androidx.transition.TransitionManager
import com.facebook.shimmer.ShimmerFrameLayout


/**
 * Created by AlexYang on 2021/8/26.
 *
 */

@set:BindingAdapter("visible")
var View.visible
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

@set:BindingAdapter("fade_in_or_out")
var View.visibleFadeInOut
    get() = visibility == View.VISIBLE
    set(value) {
        TransitionManager.beginDelayedTransition(parent as ViewGroup, Fade().apply {
            duration = 300
            addTarget(this@visibleFadeInOut)
        })
        visibility = if (value) View.VISIBLE else View.GONE
    }

@set:BindingAdapter("visible")
var View.visibleFromString: String?
    get() = ""
    set(value) {
        visibility = if (!TextUtils.isEmpty(value)) View.VISIBLE else View.GONE
    }

@set:BindingAdapter("gone")
var View.gone
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

@set:BindingAdapter("invisible")
var View.invisible
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

/**
 * View是否顯示或消失
 */
@set:BindingAdapter("visible_or_gone")
var View.visibleOrGone
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

@set:BindingAdapter("visible_or_invisible")
var View.visibleOrInvisible
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.INVISIBLE
    }

@BindingAdapter("shimmer_visible")
fun ShimmerFrameLayout.shimmerVisible(isShow: Boolean) {
    if (isShow) {
//        TransitionManager.beginDelayedTransition(parent as ViewGroup, Fade().apply {
//            duration = 300
//            addTarget(this@shimmerVisible)
//        })
        visibility = View.VISIBLE
        startShimmer()
    } else {
        if (!isVisible) return
        stopShimmer()
        visibility = View.VISIBLE
        TransitionManager.beginDelayedTransition(parent as ViewGroup, Fade().apply {
            duration = 300
            addTarget(this@shimmerVisible)
        })
        visibility = View.GONE
    }
}