package com.nownews.news2.architecture.core.ui.extensions

import android.graphics.drawable.AnimatedVectorDrawable
import android.widget.ImageView
import coil.load
import coil.size.Scale
import com.nownews.news2.architecture.core.R

/**
 * Created by AlexYang on 2021/8/2.
 *
 */


/**
 * 啟動drawable動畫
 */
val ImageView.startDrawableAnimation: Unit
    get() {
        (drawable as? AnimatedVectorDrawable)?.start()
    }

fun ImageView.coilLoad(url: String?) {
    load(url){
        crossfade(200)
        placeholder(R.drawable.img_push)
        error(R.drawable.img_push)
        fallback(R.drawable.img_push)
        scale(Scale.FILL)
    }
}
