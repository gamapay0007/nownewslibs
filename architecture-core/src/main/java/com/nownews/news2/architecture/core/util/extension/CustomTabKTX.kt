package com.nownews.news2.architecture.core.util.extension

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import com.nownews.news2.architecture.core.R
import com.nownews.news2.architecture.core.ui.extensions.colorCompat
import timber.log.Timber

/**
 * Created by AlexYang on 2022/8/23.
 *
 */
fun Context.openInCustomTab(url: String) = try {
    val tabsIntent = CustomTabsIntent.Builder()
        .setCloseButtonIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_back_white))
        .setDefaultColorSchemeParams(
            CustomTabColorSchemeParams.Builder()
                .setToolbarColor(colorCompat(R.color.color_primary))
                .setNavigationBarColor(colorCompat(R.color.color_primary))
                .build()
        )
        .setStartAnimations(
            this,
            R.anim.anim_slide_in_from_right,
            R.anim.anim_slide_out_from_right
        )
        .setExitAnimations(
            this,
            R.anim.anim_slide_in_from_left,
            R.anim.anim_slide_out_from_left
        )
        .build()
    tabsIntent.launchUrl(this, Uri.parse(url))
} catch (e: Exception) {
    Timber.i(e.localizedMessage)
}
