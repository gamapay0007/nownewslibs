package com.nownews.news2.architecture.core.util.extension

import android.os.Build

/**
 * Created by AlexYang on 2022/8/19.
 */


/**
 * 是否大於N
 */
val isN: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N

/**
 * 是否大於M
 */
val isM: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

/**
 * 是否大於O
 */
val isO: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

/**
 * 是否大於S
 * Android 31
 */
val isS: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S

/**
 * 是否大於T
 * Android 33
 */
val isT: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU