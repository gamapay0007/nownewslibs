package com.nownews.news2.architecture.core.ui.anims

import androidx.recyclerview.widget.RecyclerView
import com.facebook.rebound.SimpleSpringListener
import com.facebook.rebound.Spring
import com.facebook.rebound.SpringConfig
import com.facebook.rebound.SpringSystem
import javax.inject.Inject

/**
 * Created by AlexYang on 2021/8/27.
 *
 * RecyclerView Item 由下往上動畫
 */
val itemSpringConfig = SpringConfig(300.0, 30.0)

class SlideInBottomAnimator @Inject constructor() {
    private val springSystem = SpringSystem.create()
    private var firstInit = false
    var startItemAnimationPosition = 9
    var rvHeight = 0.0

    fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (firstInit || position <= startItemAnimationPosition) return
        holder.itemView.translationY = rvHeight.toFloat()
        springSystem.createSpring().run {
            springConfig = itemSpringConfig
            endValue = rvHeight
            addListener(object : SimpleSpringListener() {
                override fun onSpringUpdate(spring: Spring?) {
                    spring?.let {
                        holder.itemView.translationY = rvHeight.minus(it.currentValue).toFloat()
                    }
                }

                override fun onSpringEndStateChange(spring: Spring?) {
                    firstInit = false
                }
            })
        }
        startItemAnimationPosition = position
    }

    val release: Unit
        get() = springSystem.removeAllListeners()
}