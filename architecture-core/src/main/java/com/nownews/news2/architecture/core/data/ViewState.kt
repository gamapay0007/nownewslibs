package com.nownews.news2.architecture.core.data

import androidx.lifecycle.MutableLiveData
import com.nownews.news2.architecture.core.http.Errors

/**
 * Created by AlexYang on 2021/7/30.
 */
sealed class ViewState<out T>(
    val data: T? = null,
    val message: String? = null
) {
    companion object {
        fun <T> loading() = Loading<T>()

        fun <T> success(data: T) = Success(data)

        fun empty() = Empty

        fun <T> networkLostError(): ViewState<T> {
            return Error(Errors.NETWORK_ERROR())
        }

        fun <T> unknownError(): ViewState<T> {
            return Error(Errors.UNKNOWN())
        }
    }

    class Loading<T>(data: T? = null) : ViewState<T>(data)

    class Success<out T>(data: T) : ViewState<T>(data)

    class Error(val throwable: Throwable) : ViewState<Nothing>()

    object Empty : ViewState<Nothing>()

    fun isLoading(): Boolean = this is Loading
    fun isSuccess(): Boolean = this is Success
    fun isError(): Boolean = this is Error
    fun isEmpty(): Boolean = this is Empty
}

fun <T> MutableLiveData<ViewState<T>>.postSuccess(data: T) =
    postValue(ViewState.success(data))

fun <T> MutableLiveData<ViewState<T>>.postError(throwable: Throwable) =
    postValue(ViewState.Error(throwable))

fun <T> MutableLiveData<ViewState<T>>.postEmpty() =
    postValue(ViewState.empty())

fun <T> MutableLiveData<ViewState<T>>.postLoading() =
    postValue(ViewState.loading())

fun <T> ViewState<T>.handleViewState(
    doOnSubscribe: (() -> Unit)? = null,
    doOnSuccess: ((T) -> Unit)? = null,
    doOnError: ((Errors) -> Unit)? = null,
    doOnComplete: (() -> Unit)? = null
) {
    when (this) {
        is ViewState.Loading -> doOnSubscribe?.invoke()
        is ViewState.Success -> doOnSuccess?.invoke(data!!)
        is ViewState.Error -> doOnError?.invoke(throwable as Errors)
        is ViewState.Empty -> doOnComplete?.invoke()
    }
}
