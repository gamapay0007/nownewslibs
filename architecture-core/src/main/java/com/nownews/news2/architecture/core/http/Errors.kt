package com.nownews.news2.architecture.core.http

import android.net.ParseException
import com.google.gson.JsonParseException
import com.google.gson.stream.MalformedJsonException
import org.json.JSONException
import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.net.HttpURLConnection.HTTP_NO_CONTENT
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLException

/**
 * Created by AlexYang on 2021/8/8.
 *
 */
sealed class Errors(
    val code: Int = -1,
    val errorMessage: String = ""
) : Throwable() {

    data class NETWORK_ERROR(
        val _code: Int = 1000,
        val _errorMessage: String = ERROR_MESSAGE_NETWORK_LOST
    ) : Errors(_code, _errorMessage)

    data class SERVER_ERROR(
        val _code: Int = 1002,
        val _errorMessage: String = "連線伺服器失敗，請稍後重試"
    ) : Errors(_code, _errorMessage)

    data class TIMEOUT_ERROR(
        val _code: Int = 1006,
        val _errorMessage: String = "網路連線超時，請稍後重試"
    ) : Errors(_code, _errorMessage)

    data class UNKNOWN(
        val _code: Int = 1000,
        val _errorMessage: String = "未知錯誤，請稍後重試"
    ) : Errors(_code, _errorMessage)

    data class PARSE_ERROR(
        val _code: Int = 1001,
        val _errorMessage: String = "解析失敗，請稍後重試"
    ) : Errors(_code, _errorMessage)

    data class SSL_ERROR(
        val _code: Int = 1004,
        val _errorMessage: String = "憑證有誤，請稍後重試"
    ) : Errors(_code, _errorMessage)

    data class NO_CONTENT_ERROR(
        val _code: Int = HTTP_NO_CONTENT,
        val _errorMessage: String = "Response body is null"
    ) : Errors(_code, _errorMessage)

    data class ERROR(
        val _code: Int,
        val _errorMessage: String
    ) : Errors(_code, _errorMessage)

    object EmptyInputError : Errors()
    object EmptyResultsError : Errors()
    object SingleError : Errors()

    companion object {
        fun parseCode(code: Int): Errors {
            return when (code) {
                404, 500 -> SERVER_ERROR()
                else -> NETWORK_ERROR()
            }
        }
    }
}

val Throwable.throwableToErrors: Errors
    get() = when (this) {
        is HttpException, is UnknownHostException, is IOException -> Errors.NETWORK_ERROR()
        is ConnectException -> Errors.NETWORK_ERROR()
        is JsonParseException, is JSONException, is ParseException, is MalformedJsonException -> Errors.PARSE_ERROR()
        is SSLException -> Errors.SSL_ERROR()
        is SocketTimeoutException -> Errors.TIMEOUT_ERROR()
        else -> Errors.UNKNOWN()
    }

val Throwable.toErrorMessage: String
    get() = when (this) {
        is Errors -> errorMessage
        else -> localizedMessage ?: "未知錯誤"
    }

const val ERROR_MESSAGE_NETWORK_LOST = "無網路或連線不穩，請檢查您的連線狀態或稍後重試"