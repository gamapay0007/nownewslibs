package com.nownews.news2.architecture.core.ui.view

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

/**
 * Created by AlexYang on 2022/8/17.
 */
class TextClickableSpan(private val mColor: Int, private val mUnderLine: Boolean, private val mListener: TextClickableListener?) : ClickableSpan() {
    override fun onClick(widget: View) {
        mListener?.clickAction()
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.color = mColor
        if (!mUnderLine) ds.isUnderlineText = false
    }

    interface TextClickableListener {
        fun clickAction()
    }
}