package com.nownews.news2.architecture.core.util

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern


/**
 * Created by AlexYang on 2023/7/25.
 *
 * EditText with Regex
 */
class RegexInputFilter(private val patternString: String) : InputFilter {

    override fun filter(source: CharSequence?, start: Int, end: Int, p3: Spanned?, p4: Int, p5: Int): CharSequence? {
        /** 輸入合法，返回null表示不過濾 **/
        /** 輸入不合法，返回空字符串表示過濾掉输入 **/
        return if (source != null && Pattern.compile(patternString).matcher(source).matches()) null else ""
    }
}

/** 僅可輸入注音、英文、數字 **/
const val INPUT_FILTER_TYPE_GENDER = "[a-zA-Z|0-9|\u4e00-\u9fa5]+"