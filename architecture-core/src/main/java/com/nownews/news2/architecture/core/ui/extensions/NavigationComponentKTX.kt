package com.nownews.news2.architecture.core.ui.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController

/**
 * Created by AlexYang on 2021/10/28.
 *
 */
fun NavController.navigateUpOrFinish(activity: AppCompatActivity) =
    navigateUp().takeIf { true } ?: run {
        activity.finish()
        true
    }

