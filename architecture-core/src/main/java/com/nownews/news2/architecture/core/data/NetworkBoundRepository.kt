package com.nownews.news2.architecture.core.data

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.nownews.news2.architecture.core.http.Errors
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.Response

/**
 * Created by AlexYang on 2021/8/3.
 *
 */
abstract class NetworkBoundRepository<ResponseType> {

    fun asFlow() = flow<Resource<ResponseType>> {
        val apiResponse = fetchFromRemote()
        val remotePosts = apiResponse.body()
        if (apiResponse.isSuccessful && remotePosts != null) {
            handleResponse(remotePosts)
            emit(Resource.Success(remotePosts))
        } else {
//            emit(Resource.Error(apiResponse.message()))
            emit(Resource.Error(Errors.NETWORK_ERROR()))
        }
    }.catch { e ->
        e.printStackTrace()
        emit(Resource.Error(Errors.NETWORK_ERROR()))
    }

    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<ResponseType>

    @WorkerThread
    protected abstract suspend fun handleResponse(response: ResponseType)

}