package com.nownews.news2.architecture.core.ui.epoxy

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.ModelView

/**
 * Created by AlexYang on 2021/12/8.
 *
 * 垂直類型EpoxyCarousel
 */
@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT, saveViewState = true)
class LinearLayoutCarousel @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : Carousel(context, attrs, defStyleAttr) {

    override fun createLayoutManager(): LayoutManager = LinearLayoutManager(context)

    override fun getSnapHelperFactory(): SnapHelperFactory? = null

    override fun getDefaultSpacingBetweenItemsDp() = 0
}

inline fun EpoxyController.linearLayoutCarousel(modelInitializer: LinearLayoutCarouselModelBuilder.() -> Unit) {
    LinearLayoutCarouselModel_().apply { modelInitializer() }.addTo(this)
}

inline fun <T> LinearLayoutCarouselModelBuilder.withModelsFrom(
    items: List<T>,
    modelBuilder: (T, index: Int) -> EpoxyModel<*>
) {
    models(items.mapIndexed { index, t -> modelBuilder(t, index) })
}
