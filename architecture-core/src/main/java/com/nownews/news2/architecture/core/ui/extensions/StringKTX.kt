package com.nownews.news2.architecture.core.ui.extensions

import android.net.Uri
import android.text.TextUtils
import java.net.URLEncoder

/**
 * Created by AlexYang on 2021/10/7.
 *
 */

val String?.isNotEmpty: Boolean
    get() = !TextUtils.isEmpty(this)

val String.getNewsIdFromUrl: String
    get() = takeIf { it.isNotEmpty }?.let { url ->
        Uri.parse(url).lastPathSegment?.takeIf { it.isNotEmpty() }
    } ?: ""

val String.urlEncoder: String
    get() = URLEncoder.encode(this, "utf-8")
