package com.nownews.news2.architecture.core.data

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import com.nownews.news2.architecture.core.http.Errors
import kotlinx.coroutines.flow.*
import retrofit2.Response

/**
 * Created by AlexYang on 2021/8/3.
 *
 */
abstract class NetworkBoundLocalRepository<ReturnType, ResponseType> {

    fun asFlow() = flow<Resource<ReturnType>> {
        emit(Resource.Success(fetchFromLocal().first()))
        val apiResponse = fetchFromRemote()
        val remotePosts = apiResponse.body()
        if (apiResponse.isSuccessful && remotePosts != null) {
            saveRemoteData(remotePosts)
        } else {
            emit(Resource.Error(Errors.NETWORK_ERROR()))
        }
        emitAll(
            fetchFromLocal().map {
                Resource.Success<ReturnType>(it)
            }
        )
    }.catch { e ->
        e.printStackTrace()
        emit(Resource.Error(Errors.NETWORK_ERROR()))
    }

    @WorkerThread
    protected abstract suspend fun saveRemoteData(response: ResponseType)

    @MainThread
    protected abstract fun fetchFromLocal(): Flow<ReturnType>

    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<ResponseType>
}