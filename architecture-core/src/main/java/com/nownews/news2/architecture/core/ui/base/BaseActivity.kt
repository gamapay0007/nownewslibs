package com.nownews.news2.architecture.core.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavOptions

/**
 * Created by AlexYang on 2021/8/23.
 *
 */
abstract class BaseActivity<VB : ViewDataBinding> : AppCompatActivity() {
    lateinit var binding: VB

    @LayoutRes
    abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutId())
        binding.lifecycleOwner = this
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    val slideInRightAnim: NavOptions
        get() = NavOptions.Builder()
            .setEnterAnim(com.nownews.news2.architecture.core.R.anim.anim_slide_in_from_right)
            .setExitAnim(com.nownews.news2.architecture.core.R.anim.anim_slide_out_from_right)
            .setPopEnterAnim(com.nownews.news2.architecture.core.R.anim.anim_slide_in_from_left)
            .setPopExitAnim(com.nownews.news2.architecture.core.R.anim.anim_slide_out_from_left)
            .build()
}