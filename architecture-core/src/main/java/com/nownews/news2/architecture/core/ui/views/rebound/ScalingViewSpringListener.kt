package com.nownews.news2.architecture.core.ui.views.rebound

import android.view.View
import com.facebook.rebound.SimpleSpringListener
import com.facebook.rebound.Spring

/**
 * Created by AlexYang on 2021/9/7.
 *
 */
class ScalingViewSpringListener(val view: View) : SimpleSpringListener() {

    override fun onSpringUpdate(spring: Spring) {
        val value = spring.currentValue.toFloat()
        view.scaleX = value
        view.scaleY = value
    }
}