package com.nownews.news2.architecture.core.util.extension

import android.graphics.PointF
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView
import com.nownews.news2.architecture.core.ui.extensions.startDrawableAnimation

/**
 * Created by AlexYang on 2021/8/23.
 *
 */

@set:BindingAdapter("drawable_animation")
var ImageView.drawableAnimation: Boolean
    get() = false
    set(value) {
        if (value) startDrawableAnimation
    }

@BindingAdapter("fresco_load")
fun SimpleDraweeView.frescoLoad(url: String?) {
    url?.let { setImageURI(it) }
}

@BindingAdapter("fresco_load_uri")
fun SimpleDraweeView.frescoLoadUri(url: String?) {
    url?.let { setImageURI(Uri.parse(it), context) }
}

@BindingAdapter("fresco_load_big")
fun SimpleDraweeView.frescoLoadBig(url: String?) {
    hierarchy.actualImageFocusPoint = PointF(0.15f, 0.3f)
    url?.let { setImageURI(it) }
}