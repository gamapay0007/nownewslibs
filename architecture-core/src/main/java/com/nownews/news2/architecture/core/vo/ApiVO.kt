package com.nownews.news2.architecture.core.vo

/**
 * Created by AlexYang on 2021/8/6.
 *
 */

data class ApiVO<out T>(
    val code: Int,
    val msg: String?,
    val data: T?,
    val refreshKey: String? = ""
) {
    val isSuccessful: Boolean
        get() = code == 200
}
