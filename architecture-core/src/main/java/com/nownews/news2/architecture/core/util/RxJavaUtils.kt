package com.nownews.news2.architecture.core.util

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers


/**
 * Created by AlexYang on 2021/10/3.
 *
 */

fun Disposable.addTo(disposableComposite: CompositeDisposable) {
    disposableComposite.add(this)
}

/** Maybe **/
fun <T> Maybe<T>.applyIoScheduler(): Maybe<T> = applyScheduler(Schedulers.io())
fun <T> Maybe<T>.applyComputationScheduler(): Maybe<T> = applyScheduler(Schedulers.computation())
private fun <T> Maybe<T>.applyScheduler(scheduler: Scheduler) =
    subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread())

/** Flowable **/
fun <T> Flowable<T>.applyIoScheduler(): Flowable<T> = applyScheduler(Schedulers.io())
fun <T> Flowable<T>.applyComputationScheduler(): Flowable<T> = applyScheduler(Schedulers.computation())
private fun <T> Flowable<T>.applyScheduler(scheduler: Scheduler) =
    subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread())

/** Observable **/
fun <T> Observable<T>.applyIoScheduler(): Observable<T> = applyScheduler(Schedulers.io())
fun <T> Observable<T>.applyComputationScheduler(): Observable<T> = applyScheduler(Schedulers.computation())
private fun <T> Observable<T>.applyScheduler(scheduler: Scheduler) =
    subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread())

/** Single **/
fun <T> Single<T>.applyIoScheduler(): Single<T> = applyScheduler(Schedulers.io())
fun <T> Single<T>.applyComputationScheduler(): Single<T> = applyScheduler(Schedulers.computation())
private fun <T> Single<T>.applyScheduler(scheduler: Scheduler) =
    subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread())

/** Completable **/
fun Completable.applyIoScheduler(): Completable = applyScheduler(Schedulers.io())
fun Completable.applyComputationScheduler(): Completable = applyScheduler(Schedulers.computation())
private fun Completable.applyScheduler(scheduler: Scheduler) =
    subscribeOn(scheduler).observeOn(AndroidSchedulers.mainThread())

