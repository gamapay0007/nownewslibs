package com.nownews.news2.architecture.core.util.extension

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.UnderlineSpan
import androidx.annotation.ColorInt
import com.nownews.news2.architecture.core.ui.view.TextClickableSpan

/**
 * Created by AlexYang on 2022/8/17.
 *
 * KotlinExtension with SpannableStringBuilder
 */

fun SpannableStringBuilder.color(start: Int, end: Int, @ColorInt colorRes: Int) = apply {
    setSpan(ForegroundColorSpan(colorRes), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
}

fun SpannableStringBuilder.size(start: Int, end: Int, size: Float) = apply {
    setSpan(RelativeSizeSpan(size), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
}

fun SpannableStringBuilder.underline(start: Int, end: Int) = apply {
    setSpan(UnderlineSpan(), start, end, 0)
}

fun SpannableStringBuilder.click(
    start: Int,
    end: Int,
    hasUnderLine: Boolean = true,
    @ColorInt colorRes: Int,
    clickListener: () -> Unit
) = apply {
    setSpan(TextClickableSpan(colorRes, hasUnderLine, object : TextClickableSpan.TextClickableListener {
        override fun clickAction() {
            clickListener()
        }
    }), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
}
