package com.nownews.news2.architecture.core.ui.view

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.widget.EditText

/**
 * Created by AlexYang on 2023/8/4.
 *
 * EditText with Right Icon
 */
private const val COMPOUND_DRAWABLE_RIGHT_INDEX = 2

fun EditText.makeEditTextWithClear(onIsNotEmpty: (() -> Unit)?, onCleared: (() -> Unit)?) {
    compoundDrawables[COMPOUND_DRAWABLE_RIGHT_INDEX]?.let { clearDrawable ->
        makeEditTextWithClear(onIsNotEmpty, onCleared, clearDrawable)
    }
}

fun EditText.makeEditTextWithClear(
    onIsNotEmpty: (() -> Unit)?,
    onClear: (() -> Unit)?,
    clearDrawable: Drawable
) {
    val updateRightDrawable = {
        this.setCompoundDrawables(null, null, if (text.isNotEmpty()) clearDrawable else null, null)
    }
    updateRightDrawable()

    this.afterTextChanged {
        if (it.isNotEmpty()) {
            onIsNotEmpty?.invoke()
        }
        updateRightDrawable()
    }

    this.onRightDrawableClicked {
        this.text.clear()
        this.setCompoundDrawables(null, null, null, null)
        this.requestFocus()
        onClear?.invoke()
    }
}

/**
 * 設定EditText右側按鈕點擊事件
 *
 * Based on View.OnTouchListener. Be careful EditText replaces old View.OnTouchListener when setting new one
 */
@SuppressLint("ClickableViewAccessibility")
fun EditText.onRightDrawableClicked(onClicked: (view: EditText) -> Unit) {
    this.setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            if (event.x >= v.width - v.totalPaddingRight) {
                if (event.action == MotionEvent.ACTION_UP) {
                    onClicked(this)
                }
                hasConsumed = true
            }
        }
        hasConsumed
    }
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}
