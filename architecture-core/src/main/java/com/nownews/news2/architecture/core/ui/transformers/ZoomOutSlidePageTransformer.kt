package com.nownews.news2.architecture.core.ui.transformers

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs
import kotlin.math.max

/**
 * Created by AlexYang on 2021/9/5.
 *
 */
private const val MIN_SCALE = 0.85f
private const val MIN_ALPHA = 0.5f

class ZoomOutSlideTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        if (position >= -1.0f || position <= 1.0f) {
            val height = page.height.toFloat()
            val scaleFactor = max(MIN_SCALE, 1.0f - abs(position))
            val vertMargin = height * (1.0f - scaleFactor) / 2.0f
            val horMargin = page.width.toFloat() * (1.0f - scaleFactor) / 2.0f
            page.pivotY = MIN_ALPHA * height
            if (position < 0.0f) {
                page.translationX = horMargin - vertMargin / 2.0f
            } else {
                page.translationX = -horMargin + vertMargin / 2.0f
            }

            page.scaleX = scaleFactor
            page.scaleY = scaleFactor
            page.alpha = MIN_ALPHA + (scaleFactor - MIN_SCALE) / 0.14999998f * MIN_ALPHA
        }
    }
}