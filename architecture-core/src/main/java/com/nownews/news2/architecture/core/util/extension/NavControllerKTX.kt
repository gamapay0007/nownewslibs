package com.nownews.news2.architecture.core.util.extension

import androidx.annotation.IdRes
import androidx.navigation.NavOptions
import com.nownews.news2.architecture.core.R

/**
 * Created by AlexYang on 2022/8/2.
 *
 */

fun fragAnimation(
    @IdRes popUpId: Int,
    inclusive: Boolean = false,
    saveState: Boolean = false
) = NavOptions.Builder()
    .setPopUpTo(popUpId, inclusive = inclusive, saveState = saveState)
    .setEnterAnim(R.anim.anim_slide_in_from_right)
    .setExitAnim(R.anim.anim_slide_out_from_right)
    .setPopEnterAnim(R.anim.anim_slide_in_from_left)
    .setPopExitAnim(R.anim.anim_slide_out_from_left)
    .build()

fun defaultFragAnimation() =
    NavOptions.Builder()
        .setEnterAnim(R.anim.anim_slide_in_from_right)
        .setExitAnim(R.anim.anim_slide_out_from_right)
        .setPopEnterAnim(R.anim.anim_slide_in_from_left)
        .setPopExitAnim(R.anim.anim_slide_out_from_left)
        .build()
