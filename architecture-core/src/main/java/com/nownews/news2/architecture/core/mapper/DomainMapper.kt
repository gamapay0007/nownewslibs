package com.nownews.news2.architecture.core.mapper

/**
 * Created by AlexYang on 2021/8/12.
 *
 */
interface DomainMapper<Entity, DomainModel> {
    fun mapToDomainModel(entity: Entity): DomainModel
}