package com.nownews.news2.architecture.core.interceptor

import okhttp3.Interceptor
import timber.log.Timber
import java.io.IOException

/**
 * Created by AlexYang on 2021/10/9.
 *
 */
class ErrorInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain) =
        chain.proceed(chain.request()).takeIf { it.isSuccessful } ?: run {
            Timber.e("ErrorInterceptor throw error")
            throw IOException()
        }
}