package com.nownews.news2.architecture.core.util.extension

import android.text.InputFilter
import android.widget.EditText
import java.util.regex.Pattern


/**
 * Created by AlexYang on 2022/7/31.
 *
 */

/**
 * 禁止輸入特殊字元
 */
fun EditText.inhibitInputSpeChat() {
    val filter = InputFilter { source, start, end, dest, dstart, dend ->
        val speChat = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*()——+|{}【】‘；：”“'。，、？]"
        val matcher = Pattern.compile(speChat).matcher(source.toString())
        if (matcher.find()) "" else null
    }
    filters = arrayOf(filter)
}
