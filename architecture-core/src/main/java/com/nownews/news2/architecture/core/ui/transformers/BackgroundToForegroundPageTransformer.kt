package com.nownews.news2.architecture.core.ui.transformers

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs
import kotlin.math.max

/**
 * Created by AlexYang on 2021/9/5.
 *
 */
class BackgroundToForegroundPageTransformer :ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) {
        val height = page.height.toFloat()
        val width = page.width.toFloat()
        val scale = min(if (position < 0.0f) 1.0f else abs(1.0f - position), 0.5f)
        page.scaleX = scale
        page.scaleY = scale
        page.pivotX = width * 0.5f
        page.pivotY = height * 0.5f
        page.translationX = if (position < 0.0f) width * position else -width * position * 0.25f
    }

    private fun min(`val`: Float, min: Float): Float {
        return max(`val`, min)
    }

}