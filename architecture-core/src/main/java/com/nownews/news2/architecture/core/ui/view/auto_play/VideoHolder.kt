package com.nownews.news2.architecture.core.ui.view.auto_play

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by AlexYang on 2022/8/26.
 *
 * Auto Play VideoHolder
 */
abstract class VideoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun getVideoLayout(): View
    abstract fun playVideo()
    abstract fun stopVideo()
}