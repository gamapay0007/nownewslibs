package com.nownews.news2.architecture.core.ui.extensions

import android.app.Activity
import android.content.Context
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

/**
 * Created by AlexYang on 2021/8/5.
 *
 */

fun Context.string(@StringRes id: Int): String = applicationContext.getString(id)

fun Context.stringArray(@ArrayRes id: Int): Array<String> = resources.getStringArray(id)

fun Context.colorCompat(@ColorRes id: Int): Int = ContextCompat.getColor(this.applicationContext, id)


fun Activity.drawableCompat(@DrawableRes id: Int) = ContextCompat.getDrawable(this.applicationContext, id)!!
