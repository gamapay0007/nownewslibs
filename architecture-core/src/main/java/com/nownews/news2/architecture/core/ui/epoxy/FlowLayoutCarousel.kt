package com.nownews.news2.architecture.core.ui.epoxy

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.ModelView
import com.xiaofeng.flowlayoutmanager.Alignment
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager

/**
 * Created by AlexYang on 2021/10/25.
 *
 */
@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class FlowLayoutCarousel @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : Carousel(context, attrs, defStyleAttr) {

    override fun createLayoutManager(): LayoutManager =
        FlowLayoutManager().apply {
            isAutoMeasureEnabled = true
            setAlignment(Alignment.LEFT)
        }

    override fun getSnapHelperFactory(): SnapHelperFactory? = null

    override fun getDefaultSpacingBetweenItemsDp() = 0
}


inline fun EpoxyController.flowLayoutCarousel(modelInitializer: FlowLayoutCarouselModelBuilder.() -> Unit) {
    FlowLayoutCarouselModel_().apply {
        modelInitializer()
    }.addTo(this)
}

inline fun <T> FlowLayoutCarouselModelBuilder.withModelsFrom(
    items: List<T>,
    modelBuilder: (T) -> EpoxyModel<*>
) {
    models(items.map { modelBuilder(it) })
}
