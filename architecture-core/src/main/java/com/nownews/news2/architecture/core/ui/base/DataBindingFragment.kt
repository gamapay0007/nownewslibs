package com.nownews.news2.architecture.core.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import com.nownews.news2.architecture.core.R

/**
 * Created by AlexYang on 2021/9/30.
 *
 * 使用DataBinding
 */
abstract class DataBindingFragment<out VDB : ViewDataBinding> : Fragment() {
    private var _binding: ViewDataBinding? = null
    val binding: VDB
        get() = _binding as VDB

    protected abstract val bindingInflater: (LayoutInflater) -> ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater(inflater)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.unbind()
    }

    fun fragAnimation(@IdRes popUpId: Int) = NavOptions.Builder()
//        .setPopUpTo(popUpId)
        .setEnterAnim(R.anim.anim_slide_in_from_right)
        .setExitAnim(R.anim.anim_slide_out_from_right)
        .setPopEnterAnim(R.anim.anim_slide_in_from_left)
        .setPopExitAnim(R.anim.anim_slide_out_from_left)
        .build()
}