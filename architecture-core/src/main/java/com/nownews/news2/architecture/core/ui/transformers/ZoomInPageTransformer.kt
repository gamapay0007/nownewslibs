package com.nownews.news2.architecture.core.ui.transformers

import android.view.View
import androidx.viewpager2.widget.ViewPager2
import kotlin.math.abs

/**
 * Created by AlexYang on 2021/9/5.
 *
 */
class ZoomInPageTransformer : ViewPager2.PageTransformer {

    override fun transformPage(page: View, position: Float) =
        (if (position < 0.0f) position + 1.0f else abs(1.0f - position)).let { scale ->
            page.scaleX = scale
            page.scaleY = scale
            page.pivotX = page.width.toFloat() * 0.5f
            page.pivotY = page.height.toFloat() * 0.5f
            page.alpha = if (position >= -1.0f && position <= 1.0f) 1.0f - (scale - 1.0f) else 0.0f
        }
}