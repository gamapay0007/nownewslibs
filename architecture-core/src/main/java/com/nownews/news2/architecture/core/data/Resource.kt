package com.nownews.news2.architecture.core.data

import android.net.ParseException
import com.google.gson.JsonParseException
import com.google.gson.stream.MalformedJsonException
import com.nownews.news2.architecture.core.http.Errors
import com.nownews.news2.architecture.core.vo.ApiVO
import org.json.JSONException
import retrofit2.HttpException
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLException

/**
 * Created by AlexYang on 2021/8/3.
 *
 */
sealed class Resource<out T> {

    companion object {
        fun <T> success(data: T) =
            Success(data)

        fun <T> networkLostError(): Resource<T> =
            Error(Errors.NETWORK_ERROR())

        fun <T> unknownError(): Resource<T> =
            Error(Errors.UNKNOWN())
    }


    class Loading<T>(data: T? = null) : Resource<T>()

    class Success<out T>(val data: T) : Resource<T>()

    class Error(val throwable: Throwable) : Resource<Nothing>()

    object Empty : Resource<Nothing>()
}

suspend inline fun <ResponseType> parseResponse(
    crossinline doOnSubscribe: suspend () -> Response<ResponseType>,
    crossinline doOnSuccess: suspend (ResponseType) -> Resource<ResponseType>
): Resource<ResponseType> {
    return try {
        val result = doOnSubscribe()
        if (result.isSuccessful) return doOnSuccess.invoke(result.body()!!)
        Timber.e("code:${result.code()}, body: ${result.body()}")
        result.toResourceError
    } catch (e: Exception) {
        Timber.e("error-> ${e.localizedMessage}")
        e.toResourceError
    }
}

suspend inline fun <T> parseResponse(
    crossinline request: suspend () -> Response<T>
): Resource<T> {
    return try {
        request().parse()
    } catch (e: Exception) {
        e.toResourceError
    }
}

fun <T> Response<T>.parse(): Resource<T> {
    return if (isSuccessful) {
        body()?.let { Resource.Success(it) } ?: Resource.Error(Errors.NO_CONTENT_ERROR())
    } else {
        Timber.e("error= body: ${body()} ,code:${code()}")
        Resource.Error(
            Errors.ERROR(code(), errorBody()?.string() ?: message())
        )
    }
}

suspend fun <T, R> parseApiResponse(
    request: suspend () -> Response<T>,
    response: suspend (T) -> Resource<R>
): Resource<R> = try {
    request().run {
        when {
            isSuccessful -> body()?.let {
                response(it)
            }
                ?: Resource.Error(Errors.NO_CONTENT_ERROR())
            else -> {
                Timber.e("code:${code()}, body: ${body()}")
                toResourceError
            }
        }
    }
} catch (e: Exception) {
    Timber.i("error: ${e.localizedMessage}")
    e.toResourceError
}

val <T> Response<T>.toResourceError: Resource.Error
    get() {
        return Resource.Error(
            when (code()) {
                404, 500 -> Errors.SERVER_ERROR()
                else -> Errors.ERROR(code(), errorBody()?.string() ?: run {
                    message().takeIf { it.isNotEmpty() } ?: "errorCode: ${code()}"
                })
            }
        )
    }

val <T> ApiVO<T>.toResourceError: Resource.Error
    get() {
        return Resource.Error(
            when (code) {
                404, 500 -> Errors.SERVER_ERROR()
                else -> Errors.ERROR(code, msg?.takeIf { it.isNotEmpty() } ?: "errorCode: $code")
            }
        )
    }

val Throwable.toResourceError: Resource.Error
    get() = Resource.Error(
        when (this) {
            is HttpException, is UnknownHostException, is IOException -> Errors.NETWORK_ERROR()
            is ConnectException -> Errors.NETWORK_ERROR()
            is JsonParseException, is JSONException, is ParseException, is MalformedJsonException -> Errors.PARSE_ERROR()
            is SSLException -> Errors.SSL_ERROR()
            is SocketTimeoutException -> Errors.TIMEOUT_ERROR()
            else -> Errors.UNKNOWN()
        }
    )

//    suspend fun <T, R> processApiResponse(
//        doOnSubscribe: suspend () -> Response<T>,
//        doOnSuccess: suspend (T) -> ViewState<R>
//    ): ViewState<R> {
//        return try {
//            val response = doOnSubscribe()
//            val responseCode = response.code()
////            val responseMessage = response.message()
//            if (response.isSuccessful) {
//                doOnSuccess.invoke(response.body()!!)
//            } else {
//                Timber.e("error= body: ${response.errorBody().toString()} ,code:${responseCode}")
//                ViewState.failureCode(responseCode)
//            }
//        } catch (e: Exception) {
//            ViewState.toFailureType(e)
//        }
//    }