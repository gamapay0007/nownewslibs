package com.nownews.news2.architecture.core.ui.bindings

import android.text.TextUtils
import android.util.TypedValue
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import coil.load

/**
 * Created by AlexYang on 2021/8/20.
 *
 */

@BindingAdapter("load")
fun ImageView.loadResource(url: String) {
    load(url)
}

@BindingAdapter("load_res")
fun ImageView.loadRes(@DrawableRes drawableRes: Int) {
    load(drawableRes)
}

@BindingAdapter("resource")
fun AppCompatImageView.loadResource(@DrawableRes drawableRes: Int) =
    setImageDrawable(ContextCompat.getDrawable(context.applicationContext, drawableRes))

@BindingAdapter("text_size")
fun TextView.textSize(value: Float) {
    setTextSize(TypedValue.COMPLEX_UNIT_DIP, value)
}

/**
 * 設定HTML格式的文字
 */
@BindingAdapter("html_text")
fun TextView.htmText(value: String?) {
    text = if (!TextUtils.isEmpty(value)) HtmlCompat.fromHtml(value!!, HtmlCompat.FROM_HTML_MODE_LEGACY) else ""
}

