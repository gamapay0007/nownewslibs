package com.nownews.news2.architecture.core.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.ModelView

/**
 * Created by AlexYang on 2024/2/21.
 *
 * Grid_垂直型EpoxyCarousel
 */
@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT, saveViewState = true)
class GridLayoutCarousel @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : Carousel(context, attrs, defStyleAttr) {

    override fun createLayoutManager(): LayoutManager =
        GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)

    override fun getSnapHelperFactory(): SnapHelperFactory? = null

    override fun getDefaultSpacingBetweenItemsDp() = 0

    fun setSpanCount(count: Int = 2) {
        (layoutManager as GridLayoutManager).spanCount = count
    }
}

inline fun EpoxyController.gridLayoutCarousel(
    spanCount: Int,
    modelInitializer: GridLayoutCarouselModelBuilder.() -> Unit
) {
    GridLayoutCarouselModel_().onBind { _, view, _ ->
        view.setSpanCount(spanCount)
    }.apply { modelInitializer() }.addTo(this)
}

inline fun <T> GridLayoutCarouselModelBuilder.withModelsFrom(
    items: List<T>,
    modelBuilder: (T, index: Int) -> EpoxyModel<*>
) {
    models(items.mapIndexed { index, t -> modelBuilder(t, index) })
}