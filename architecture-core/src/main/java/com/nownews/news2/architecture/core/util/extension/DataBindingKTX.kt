package com.nownews.news2.architecture.core.util.extension

import android.content.Context
import android.view.LayoutInflater
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * Created by AlexYang on 2022/8/2.
 *
 */

@Suppress("UNCHECKED_CAST")
fun <T> Context.dataBinding(@LayoutRes layoutRes: Int): T = DataBindingUtil.inflate<ViewDataBinding>(
    LayoutInflater.from(this), layoutRes, null, false) as T

