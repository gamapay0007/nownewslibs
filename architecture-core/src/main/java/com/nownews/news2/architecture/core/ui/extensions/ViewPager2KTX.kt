package com.nownews.news2.architecture.core.ui.extensions

import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

/**
 * Created by AlexYang on 2021/10/2.
 *
 */

/**
 * 減少ViewPager2左右滑動敏感度
 */
val ViewPager2.reduceDragSensitivity: Unit
    get() {
        val recyclerView = ViewPager2::class.java.getDeclaredField("mRecyclerView").apply { isAccessible = true }.get(this) as RecyclerView
        val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop").apply { isAccessible = true }
        touchSlopField.set(recyclerView, (touchSlopField.get(recyclerView) as Int) * 4)
    }

fun ViewPager2.reduceDragSensitivity(distance: Int) {
    val recyclerView = ViewPager2::class.java.getDeclaredField("mRecyclerView").apply { isAccessible = true }.get(this) as RecyclerView
    val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop").apply { isAccessible = true }
    touchSlopField.set(recyclerView, (touchSlopField.get(recyclerView) as Int) * distance)
}