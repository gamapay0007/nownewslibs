package com.nownews.news2.architecture.core.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.nownews.news2.architecture.core.util.extension.isM
import javax.inject.Inject

/**
 * 檢查網路連線狀態
 */
class NetworkManager @Inject constructor(private val ctx: Context) {

    suspend inline fun <R> handleNetworkConnected(
        crossinline doOnAvailable: suspend () -> R,
        crossinline doOnLost: suspend () -> R
    ) {
        if (isNetworkConnected()) doOnAvailable() else doOnLost()
    }

    fun isNetworkConnected(): Boolean {
        var result = false
        val connectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (isM) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }

        return result
    }
}