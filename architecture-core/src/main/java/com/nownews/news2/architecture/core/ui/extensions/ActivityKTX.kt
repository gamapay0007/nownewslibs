package com.nownews.news2.architecture.core.ui.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.core.content.pm.PackageInfoCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.nownews.news2.architecture.core.R
import java.util.*

/**
 * Created by AlexYang on 2021/8/1.
 *
 * Activity Extension
 */

sealed class ActivityAnim {
    object FadeInOut : ActivityAnim()
    object SlideRightLeft : ActivityAnim()
}

/**
 * 開啟Activity with Bundle
 *
 * ref:https://wajahatkarim.com/2019/03/-launching-activities-in-easier-way-using-kotlin-extensions-/
 */
inline fun <reified T : Any> Activity.openActivity(
    requestCode: Int = -1,
    options: Bundle? = null,
    anim: ActivityAnim = ActivityAnim.FadeInOut,
    noinline init: Intent.() -> Unit = {}
) {
    startActivityForResult(newIntent<T>(this).also(init), requestCode, options)
//    when (anim) {
////        is ActivityAnim.FadeInOut -> fadeInFadeOut
//        is ActivityAnim.SlideRightLeft -> slideEnter
//    }
    slideEnter
}

/**
 * 開啟Activity
 */
inline fun <reified T : Any> Context.openActivity(
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}
) {
    startActivity(newIntent<T>(this).also(init), options)
    (this as? Activity)?.fadeInFadeOut
}

inline fun <reified T : Any> newIntent(context: Context): Intent =
    Intent(context, T::class.java)

val Activity.fadeInFadeOut: Unit
    get() = overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

val Activity.slideEnter: Unit
    get() = overridePendingTransition(R.anim.anim_slide_in_from_right, R.anim.anim_slide_out_from_right)

val Activity.slideExit: Unit
    get() = overridePendingTransition(R.anim.anim_slide_in_from_left, R.anim.anim_slide_out_from_left)


/**
 * 透過url開啟Browser
 */
fun Activity.startBrowser(url: String) = startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

/**
 * Start share compat
 */
fun Activity.startShareIntent(title: String = "", content: String?) {
    if (TextUtils.isEmpty(content)) return
    ShareCompat.IntentBuilder(this)
        .setType("text/plain")
        .setChooserTitle(title)
        .setText(content)
        .startChooser()
}

val Activity.startGooglePlay: Unit
    get() {
        try {
            startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")).apply {
                    setPackage("com.android.vending")
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }
            )
        } catch (e: Exception) {
            startBrowser("https://play.google.com/store/apps/details?id=$packageName")
        }
    }

fun <T> Context.dataBinding(@LayoutRes layoutRes: Int, parent: ViewGroup): T = DataBindingUtil.inflate<ViewDataBinding>(
    LayoutInflater.from(this), layoutRes, parent, false) as T

fun Context.dp2px(dp: Int) = dp.times(resources.displayMetrics.density).toInt()

fun Context.px2dp(px: Int) = px.div(resources.displayMetrics.density).toInt()

/**
 * 關閉鍵盤
 */
fun FragmentActivity.hideKeyboard() {
    val inputMethodManager = getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
    if (window.attributes.softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
        if (currentFocus != null) inputMethodManager.hideSoftInputFromWindow(
            currentFocus?.windowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }
}

/**
 * 在Fragment關閉Keyboard
 */
val Fragment.hideKeyboardInFragment: Unit
    get() {
        activity?.let { act ->
            (act.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).let { imm ->
                this@hideKeyboardInFragment.view?.rootView?.windowToken?.let { token ->
                    imm.hideSoftInputFromWindow(token, InputMethodManager.RESULT_UNCHANGED_SHOWN)
                }
            }
        }
    }

/**
 * 顯示版本號.版號Code
 */
val Activity.appVersion: String
    get() {
        packageManager.getPackageInfo(packageName, 0).let {
            return "v %s".format(
                Locale.getDefault(),
                it.versionName.plus(".").plus(PackageInfoCompat.getLongVersionCode(it))
            )
        }
    }

/** 檢查裝置是否為靜音模式 **/
fun Context.detectMuteModel(): Boolean =
    (getSystemService(Context.AUDIO_SERVICE) as AudioManager).let { manager ->
        return manager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0
    }