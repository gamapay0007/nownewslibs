package com.nownews.news2.architecture.core.ui.extensions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by AlexYang on 2021/8/5.
 *
 */

object GsonKTX {
    val INSTANCE: Gson = Gson()
}

inline fun <reified T> String.fromJson(): T {
    return GsonKTX.INSTANCE.fromJson(this, object : TypeToken<T>() {}.type)
}

inline val Any.toJson: String
    get() = GsonKTX.INSTANCE.toJson(this)