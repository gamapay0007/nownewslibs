package com.nownews.news2.architecture.core.ui.extensions

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

/**
 * Created by AlexYang on 2021/10/17.
 *
 */

fun SwipeRefreshLayout.pullDistance(dp: Int) {
    setDistanceToTriggerSync(context.dp2px(dp))
}
