package com.nownews.news2.architecture.core.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding

/**
 * Created by AlexYang on 2021/9/30.
 *
 */
abstract class DataBindingActivity<out VDB : ViewDataBinding> : AppCompatActivity() {
    private var _binding: ViewDataBinding? = null
    protected val binding: VDB
        get() = _binding as VDB

    protected abstract val bindingInflater: (LayoutInflater) -> ViewDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = bindingInflater(layoutInflater)
        binding.lifecycleOwner = this
        setContentView(binding.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }
}