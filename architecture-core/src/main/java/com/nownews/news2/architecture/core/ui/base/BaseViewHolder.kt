package com.nownews.news2.architecture.core.ui.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by AlexYang on 2021/8/24.
 *
 */
abstract class BaseViewHolder<T : ViewDataBinding>(
    _binding: T
) : RecyclerView.ViewHolder(_binding.root)