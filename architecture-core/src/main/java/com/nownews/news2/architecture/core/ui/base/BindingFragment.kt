package com.nownews.news2.architecture.core.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController

/**
 * Created by AlexYang on 2021/9/4.
 *
 * 使用DataBinding
 */
abstract class BindingFragment<VDB : ViewDataBinding>(@LayoutRes val layoutID: Int) : Fragment() {
    private var _binding: VDB? = null
    val binding by lazy { _binding!! }

    abstract fun initViews()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = DataBindingUtil.inflate(inflater, layoutID, container, false)
        binding.lifecycleOwner = this
        initViews()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.unbind()
        _binding = null
    }

    fun NavController.popAllTo(@IdRes destinationId: Int) {
        popBackStack(destinationId, inclusive = false)
    }
}