package com.nownews.news2.architecture.core.util

import java.util.*

/**
 * Created by AlexYang on 2021/9/15.
 *
 */
object TimeUtils {
    private const val minute = (60 * 1000).toLong()
    private const val hour = 60 * minute // 1小時
    private const val day = 24 * hour // 1天
    private const val month = 31 * day // 月
    private const val year = 12 * month // 年

    /**
     * 設定幾分鐘前
     *
     * @param date
     * @return
     */
    fun timeAgo(date: Date): String = (Date().time - date.time).let { diff ->
        when {
            diff > year -> diff.div(year).toString().plus(" 年前")
            diff > month -> diff.div(month).toString().plus(" 月前")
            diff > day -> diff.div(day).toString().plus(" 天前")
            diff > hour -> diff.div(hour).toString().plus(" 小時前")
            diff > minute -> diff.div(minute).toString().plus(" 分鐘前")
            else -> "剛剛"
        }
    }
}