package com.nownews.news2.architecture.core.ui.view.auto_play;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.PublishSubject;


/**
 * Created by AlexYang on 2022/8/26.
 *
 * Auto Play RecyclerView
 */
public class AutoPlayVideoRecyclerView extends RecyclerView {
    private PublishSubject<Integer> subject;
    private VideoHolder handingVideoHolder;
    private int handingPosition = 0;
    private int newPosition = -1;

    private final int heightScreen = getResources().getDisplayMetrics().heightPixels;

    public AutoPlayVideoRecyclerView(Context context) {
        super(context);
        initView();
    }

    public AutoPlayVideoRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AutoPlayVideoRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public VideoHolder getHandingVideoHolder() {
        return handingVideoHolder;
    }

    private void initView() {
        subject = createSubject();
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                checkPositionHandingViewHolder();
                subject.onNext(dy);
            }
        });

    }

    private void checkPositionHandingViewHolder() {
        if (handingVideoHolder == null) return;
        Observable.just(handingVideoHolder)
                .map(this::getPercentViewHolderInScreen)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Float>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {}

                    @Override
                    public void onNext(@NonNull Float aFloat) {
                        if (aFloat < 50 && handingVideoHolder != null) {
                            handingVideoHolder.stopVideo();
                            handingVideoHolder = null;
                            handingPosition = -1;
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

    private PublishSubject<Integer> createSubject() {
        subject = PublishSubject.create();
        subject.debounce(300, TimeUnit.MILLISECONDS)
                .filter(value -> true)
                .switchMap((Function<Integer, ObservableSource<Integer>>) Observable::just)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::playVideo);
        return subject;
    }

    private void playVideo(float value) {
        Observable.just(value)
                .map(aFloat -> {
                    VideoHolder videoHolder = getViewHolderCenterScreen();
                    if (videoHolder == null) return null;
                    if (videoHolder.equals(handingVideoHolder) && handingPosition == newPosition) return null;
                    handingPosition = newPosition;
                    return videoHolder;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VideoHolder>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {}

                    @Override
                    public void onNext(@NonNull VideoHolder videoHolder) {
                        if (handingVideoHolder != null) handingVideoHolder.stopVideo();
                        videoHolder.playVideo();
                        handingVideoHolder = videoHolder;
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {}
                    @Override
                    public void onComplete() {}
                });
    }

    private VideoHolder getViewHolderCenterScreen() {
        int[] limitPosition = getLimitPositionInScreen();
        int min = limitPosition[0];
        int max = limitPosition[1];

        VideoHolder viewHolderMax = null;
        float percentMax = 0;

        for (int i = min; i <= max; i++) {
            ViewHolder viewHolder = findViewHolderForAdapterPosition(i);
            if (!(viewHolder instanceof VideoHolder)) continue;
            float percentViewHolder = getPercentViewHolderInScreen((VideoHolder) viewHolder);
            if (percentViewHolder > percentMax && percentViewHolder >= 50) {
                percentMax = percentViewHolder;
                viewHolderMax = (VideoHolder) viewHolder;
                newPosition = i;
            }
        }
        return viewHolderMax;
    }

    private float getPercentViewHolderInScreen(VideoHolder viewHolder) {
        if (viewHolder == null) return 0;
        View view = viewHolder.getVideoLayout();

        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int viewHeight = view.getHeight();
        int viewFromY = location[1];
        int viewToY = location[1] + viewHeight;

        if (viewFromY >= 0 && viewToY <= heightScreen) return 100;
        if (viewFromY < 0 && viewToY > heightScreen) return 100;
        if (viewFromY < 0 && viewToY <= heightScreen)
            return ((float) (viewToY - (-viewFromY)) / viewHeight) * 100;
        if (viewFromY >= 0 && viewToY > heightScreen)
            return ((float) (heightScreen - viewFromY) / viewHeight) * 100;
        return 0;
    }

    private int[] getLimitPositionInScreen() {
        int findFirstVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
        int findFirstCompletelyVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        int findLastVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findLastVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findLastCompletelyVisibleItemPosition();

        int min = Math.min(Math.min(findFirstVisibleItemPosition, findFirstCompletelyVisibleItemPosition),
                Math.min(findLastVisibleItemPosition, findLastCompletelyVisibleItemPosition));
        int max = Math.max(Math.max(findFirstVisibleItemPosition, findFirstCompletelyVisibleItemPosition),
                Math.max(findLastVisibleItemPosition, findLastCompletelyVisibleItemPosition));

        return new int[]{min, max};
    }
}