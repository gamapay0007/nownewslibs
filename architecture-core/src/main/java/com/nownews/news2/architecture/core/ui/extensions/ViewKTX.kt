package com.nownews.news2.architecture.core.ui.extensions

import android.animation.AnimatorInflater
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nownews.news2.architecture.core.R


/**
 * Created by AlexYang on 2021/8/2.
 *
 * View Extension
 */


/**
 * 顯示View
 */
val View.visible: Unit
    get() {
        visibility = View.VISIBLE
    }

/**
 * 隱藏View
 */
val View.invisible: Unit
    get() {
        visibility = View.INVISIBLE
    }

/**
 * 隱藏View
 */
val View.gone: Unit
    get() {
        visibility = View.GONE
    }

fun View.visibleOrGone(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

/**
 * 設定當View可見時顯示淡入動畫
 */
inline val View.fadeIn: Unit
    get() {
        visibility = View.VISIBLE
        AnimatorInflater.loadAnimator(context, R.animator.animator_fade_in).run {
            setTarget(this@fadeIn)
            start()
        }
    }

inline val View.fadeOut: Unit
    get() {
        visibility = View.VISIBLE
        AnimatorInflater.loadAnimator(context, R.animator.animator_fade_out).run {
            setTarget(this@fadeOut)
            start()
        }
    }

fun View.snackBar(message: String, duration: Int = BaseTransientBottomBar.LENGTH_LONG) =
    Snackbar.make(this, message, duration).apply {
        /** 文字水平置中 **/
        view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).textAlignment =
            View.TEXT_ALIGNMENT_CENTER
    }.show()

fun <T : View> T.click(action: (T) -> Unit) {
    setOnClickListener {
        action(this)
    }
}

/**
 * 帶有限制快數點擊的點擊事件
 */
fun <T : View> T.singleClick(interval: Long = 500L, action: ((T) -> Unit)?) {
    setOnClickListener(SingleClickListener(interval, action))
}

class SingleClickListener<T : View>(
    private val interval: Long = 500L,
    private var clickFunc: ((T) -> Unit)?
) : View.OnClickListener {
    private var lastClickTime = 0L

    override fun onClick(v: View) {
        val nowTime = System.currentTimeMillis()
        if (nowTime - lastClickTime > interval) {
            clickFunc?.invoke(v as T)
            lastClickTime = nowTime
        }
    }
}

val View.heightPixel: Int
    get() = resources.displayMetrics.heightPixels

