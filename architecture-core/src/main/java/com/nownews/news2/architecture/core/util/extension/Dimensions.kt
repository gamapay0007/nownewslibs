package com.nownews.news2.architecture.core.util.extension

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import androidx.annotation.DimenRes
import androidx.fragment.app.Fragment

/**
 * Created by alexyang on 2020-02-18.
 *
 * KotlinExtension with Dimension
 */

val Int.dp: Int
    get() = div(Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
    get() = times(Resources.getSystem().displayMetrics.density).toInt()

val Activity.screenWidth: Int
    get() = resources.displayMetrics.widthPixels

val Activity.screenWidthToDpWithFloat: Float
    get() = resources.displayMetrics.run { widthPixels.div(density) }

val Fragment.screenWidthToDpWithFloat: Float
    get() = resources.displayMetrics.run { widthPixels.div(density) }

fun Context.dimension(@DimenRes id: Int) = resources.getDimension(id).toInt()

fun Context.dimensionPixelSize(@DimenRes dimen: Int): Int = resources.getDimensionPixelSize(dimen)

/**
 * Extension method to find a device width in pixels
 */
val Context.displayWidth: Int
    get() = resources.displayMetrics.widthPixels

/**
 * Extension method to find a device height in pixels
 */
val Context.displayHeight: Int
    get() = resources.displayMetrics.heightPixels